package com.example.viewpg.fragments

import android.content.Context.MODE_PRIVATE
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.example.viewpg.R

class FirstFragment: Fragment(R.layout.fragment_first) {

    private lateinit var editTextNote: EditText
    private lateinit var textViewNote: TextView
    private lateinit var buttonAddNote: Button

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        init(view)

        val sharedPreferences = requireActivity().getSharedPreferences("Notes", MODE_PRIVATE)
        textViewNote.text = sharedPreferences.getString("note", "")

        buttonAddNote.setOnClickListener {
            val note = textViewNote.text.toString()
            val text = editTextNote.text.toString()
            val result = text + "\n" + note
            textViewNote.text = result

            sharedPreferences.edit()
                .putString("note", result)
                .apply()

            editTextNote.text.clear()
        }
    }

    private fun init(view: View) {
        editTextNote = view.findViewById(R.id.editTextNote)
        textViewNote = view.findViewById(R.id.textViewNote)
        buttonAddNote = view.findViewById(R.id.buttonAddNote)
    }

}